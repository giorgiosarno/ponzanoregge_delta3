#include <math.h>
#include <mpfr.h>
#include <omp.h>
#include <stdio.h>
#include <string.h>

#include "sl2cfoam.h"
#include "common.h"
#include "wigxjpf.h"
#include "config.h"
#include "utilities.h"
#include "jsymbols.h"

int main(int argc, char **argv) {

	// Prepare library
	struct sl2cfoam_config libconf;
	libconf.data_folder = "../data_sl2cfoam/";
	libconf.store_ampls = 0;
	libconf.verbosity = 0;
	libconf.coupling = SL2CFOAM_COUPLING_REDUCIBLE;
	
	dspin two_j1, two_j2, two_j3, 
		  two_j4, two_j5, two_j6,
		  two_j7, two_j8, two_j9;

	two_j1 = atoi(argv[1]);	  
	two_j2 = atoi(argv[2]);	  
	two_j3 = atoi(argv[3]);	  
	two_j4 = atoi(argv[4]);	  
	two_j5 = atoi(argv[5]);	  
	two_j6 = atoi(argv[6]);	  
	two_j7 = atoi(argv[7]);	  
	two_j8 = atoi(argv[8]);	  
	two_j9 = atoi(argv[9]);

	dspin two_N_min = atoi(argv[10]);
	dspin two_N_max = atoi(argv[11]);

	// Paths for tables
	char path_data[1024];
	
	if (get_coupling() == SL2CFOAM_COUPLING_IRREDUCIBLE) {

		sprintf(path_data, "../delta3/data_delta3/delta3_asym3_bf_irr.%i.%i.%i.%i.%i.%i.%i.%i.%i.txt", 
				two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9);

		//  and initialize wigxjp 
		wig_table_init(2*MAX_J, 9);
		wig_temp_init(2*MAX_J);	

	} else {
		sprintf(path_data, "../delta3/data_delta3/delta3_asym3_bf_red.%i.%i.%i.%i.%i.%i.%i.%i.%i.txt", 
				two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9);

		//  and initialize wigxjp 
		wig_table_init(2*MAX_J, 6);
		wig_temp_init(2*MAX_J);

	}

	FILE *data = fopen(path_data, "w");

	for(dspin two_N = two_N_min; two_N <= two_N_max; two_N ++){

		dspin two_j1_N, two_j2_N, two_j3_N, 
			two_j4_N, two_j5_N, two_j6_N,
			two_j7_N, two_j8_N, two_j9_N;
		
		two_j1_N = two_j1 * two_N;	
		two_j2_N = two_j2 * two_N;	  
		two_j3_N = two_j3 * two_N;	  
		two_j4_N = two_j4 * two_N;	  
		two_j5_N = two_j5 * two_N;	  
		two_j6_N = two_j6 * two_N;	  
		two_j7_N = two_j7 * two_N;	  
		two_j8_N = two_j8 * two_N;	  
		two_j9_N = two_j9 * two_N;	

		// compute the su(2) amplitude
		// namely the 9j symbol (reducible)
		// or irreducible
		double wig9j; 

		if (get_coupling() == SL2CFOAM_COUPLING_IRREDUCIBLE) {

			wig9j = wig9jj(two_j1_N, two_j2_N, two_j3_N,
						   two_j4_N, two_j5_N, two_j6_N,
						   two_j7_N, two_j8_N, two_j9_N);

		} else {

			wig9j = wig6jj(two_j1_N, two_j2_N, two_j3_N,
						   two_j4_N, two_j5_N, two_j6_N)
				  * wig6jj(two_j1_N, two_j2_N, two_j3_N,
						   two_j7_N, two_j8_N, two_j9_N);

		}	
		
		fprintf(data, "%i %17g \n", two_N, wig9j);
	}

	fclose(data);

	wig_temp_free(); 
	wig_table_free();

	return EXIT_SUCCESS;

}
