/* Copyright 2019 Giorgio Sarno, Pietro Donà and Francesco Gozzini */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

// tool to compute the distribution of the summands
// in the sum of the irreducible and reducible 9j symbols

#include <math.h>
#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>

#include "sl2cfoam.h"
#include "common.h"
#include "error.h"
#include "dbg.h"
#include "utilities.h"
#include "wigxjpf.h"

#define REDUCIBLE_9J   0
#define IRREDUCIBLE_9J 1

#define MAX_J_WIG 4000


static double wig9jj_x(dspin two_j1, dspin two_j2, dspin two_j3,
					   dspin two_j4, dspin two_j5, dspin two_j6,
					   dspin two_j7, dspin two_j8, dspin two_j9,
					   dspin two_x, int coupling) {

	double res;

	double prefactor;
	double bf_1, bf_2, bf_3;

	if (coupling == IRREDUCIBLE_9J) {

		prefactor = d(two_x) * real_negpow(2 * two_x);

		bf_1 = wig6jj(two_j1, two_j2, two_j3,
					  two_j6, two_j9, two_x);

		bf_2 = wig6jj(two_j4, two_j5, two_j6,
				      two_j2, two_x, two_j8);

		bf_3 = wig6jj(two_j7, two_j8, two_j9,
					  two_x, two_j1, two_j4);

		res = prefactor * bf_1 * bf_2 * bf_3;					  					  					  

	} else {

		// use Biedenharn-Elliot formula

		prefactor = d(two_x) * real_negpow(two_x + 
									       two_j1 + two_j2 + two_j3 +
									       two_j4 + two_j5 + two_j6 +
		 								   two_j7 + two_j8 + two_j9);

		bf_1 = wig6jj(two_j5, two_j8, two_x,
					  two_j9, two_j6, two_j1);
		
		bf_2 = wig6jj(two_j9, two_j6, two_x,
					  two_j4, two_j7, two_j2);

		bf_3 = wig6jj(two_j4, two_j7, two_x,
					  two_j8, two_j5, two_j3);

		res = prefactor * bf_1 * bf_2 * bf_3;

	}

	return res;

}

int main(int argc, char **argv) {

	if (argc < 3) {
        fprintf(stderr, "Usage: %s [-v] [-i] [-s] [dspin] [-l] [lmin,lmax,lstep] -o [out folder]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

	// spins
	dspin two_j1, two_j2, two_j3, 
		  two_j4, two_j5, two_j6,
		  two_j7, two_j8, two_j9;

	// lambdas
	int lambda_min, lambda_max, lambda_step;
	lambda_step = -1;

    // verbose flag
    bool verbose;
    verbose = false;

	// coupling
	int coupling = REDUCIBLE_9J;

	char filepath[1024];
	strcpy(filepath, "./delta3/"); // default directory

    /////////////////////////////////////////////////////////////////////
    // flag parsing
    /////////////////////////////////////////////////////////////////////

    int opt;
    dspin tj;
    while ((opt = getopt(argc, argv, "vis:l:o:")) != -1) {
        switch (opt) {
        case 'v': verbose = true; break;
        case 'i': coupling = IRREDUCIBLE_9J; break;
        case 's': // set spin individually
            // sscanf(optarg, "%u,%u,%u,%u,%u,%u,%u,%u,%u", 
            sscanf(optarg, "%hu,%hu,%hu,%hu,%hu,%hu,%hu,%hu,%hu", 
                   &two_j1, &two_j2, &two_j3, &two_j4, &two_j5,
                   &two_j6, &two_j7, &two_j8, &two_j9);
            
            break;
		case 'l': // set lambda
            sscanf(optarg, "%d,%d,%d", 
                   &lambda_min, &lambda_max, &lambda_step);
            break;
        case 'o':
            strcpy(filepath, optarg);
            strcat(filepath, "/");
            break;
        default:
            fprintf(stderr, "Usage: %s [-v] [-i] [-s] [dspin] [-l] [lmin,lmax,lstep] -o [out folder]\n", argv[0]);
            exit(EXIT_FAILURE);
        }
    }

	if (lambda_step < 0) {
		fprintf(stderr, "Usage: %s [-v] [-i] [-s] [dspin] [-l] [lmin,lmax,lstep] -o [out folder]\n", argv[0]);
        exit(EXIT_FAILURE);
	}

	// initial checks for tr inequalities and integrity
	// in the reducible case
	if (coupling == REDUCIBLE_9J) {

		ensure_triangle    (two_j1, two_j2, two_j3);
		ensure_integer_3sum(two_j1, two_j2, two_j3);

		ensure_triangle    (two_j1, two_j5, two_j6);
		ensure_integer_3sum(two_j1, two_j5, two_j6);
		ensure_triangle    (two_j4, two_j2, two_j6);
		ensure_integer_3sum(two_j4, two_j2, two_j6);
		ensure_triangle    (two_j4, two_j5, two_j3);
		ensure_integer_3sum(two_j4, two_j5, two_j3);

		ensure_triangle    (two_j1, two_j8, two_j9);
		ensure_integer_3sum(two_j1, two_j8, two_j9);
		ensure_triangle    (two_j7, two_j2, two_j9);
		ensure_integer_3sum(two_j7, two_j2, two_j9);
		ensure_triangle    (two_j7, two_j8, two_j3);
		ensure_integer_3sum(two_j7, two_j8, two_j3);

	}

	// initialize wigxjpf
	wig_table_init(2*MAX_J_WIG, 6);
	wig_temp_init(2*MAX_J_WIG);

	for (dspin lambda = lambda_min; lambda <= lambda_max; lambda += lambda_step) {

		if (verbose) {
			printf("Computing lambda = %i ...", lambda);
			fflush(stdout);
		}

		dspin two_j1_l, two_j2_l, two_j3_l, 
			  two_j4_l, two_j5_l, two_j6_l,
		      two_j7_l, two_j8_l, two_j9_l;
		
		two_j1_l = two_j1 * lambda;	
		two_j2_l = two_j2 * lambda;	  
		two_j3_l = two_j3 * lambda;	  
		two_j4_l = two_j4 * lambda;	  
		two_j5_l = two_j5 * lambda;	  
		two_j6_l = two_j6 * lambda;	  
		two_j7_l = two_j7 * lambda;	  
		two_j8_l = two_j8 * lambda;	  
		two_j9_l = two_j9 * lambda;

		// fileath for this lambda
		// filename format "d3-dist3-bf_%i.%i.%i.%i.%i.%i.%i.%i.%i_%i_IRR/RED.dat"
		char filepath_l[1024];
		char filename[128];

		if (coupling == IRREDUCIBLE_9J) {
			sprintf(filename, "d3-dist3-bf_%i.%i.%i.%i.%i.%i.%i.%i.%i_%i_IRR.dat", 
					two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, lambda);
		} else {
			sprintf(filename, "d3-dist3-bf_%i.%i.%i.%i.%i.%i.%i.%i.%i_%i_RED.dat", 
					two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, lambda);
		}

		strcpy(filepath_l, filepath);
		strcat(filepath_l, filename);

		FILE *file = fopen(filepath_l, "w");
		if (file == NULL){
			error("error opening file for writing");
		}

		// x boundaries
		dspin two_x_min, two_x_max;

		if (coupling == IRREDUCIBLE_9J) {

			two_x_min = max(max(abs(two_j1_l-two_j9_l),abs(two_j2_l-two_j6_l)),abs(two_j8_l-two_j4_l));
			two_x_max = min(min(two_j1_l+two_j9_l,two_j2_l+two_j6_l),two_j8_l+two_j4_l);

		} else {

			two_x_min = max(max(abs(two_j5_l-two_j8_l),abs(two_j9_l-two_j6_l)),abs(two_j4_l-two_j7_l));
			two_x_max = min(min(two_j5_l+two_j8_l,two_j9_l+two_j6_l),two_j4_l+two_j7_l);

		}

		// buffer array
		size_t nx = (two_x_max - two_x_min) / 2 + 1;
		double* buf = calloc(nx, sizeof(double));

		int nthreads;
		size_t x_per_thread;
		size_t x_last;
		int filled = 0;

		#pragma omp parallel reduction (+:filled)
        {

		wig_thread_temp_init(MAX_J_WIG);

		#pragma omp single
		{

		// get number of threads
		nthreads = omp_get_num_threads();

		// get the block of xs per each thread
		// there will be x missing to be filled at the end (division with remainder...)
		x_per_thread = nx / nthreads;
		x_last = nx % nthreads;

		}

		#pragma omp for
		for (int thread_id = 0; thread_id < nthreads; thread_id++) {

			dspin two_x_min_thread = two_x_min + 2*thread_id*x_per_thread;
			dspin two_x_max_thread = two_x_min_thread + 2*x_per_thread;

			int bufi = x_per_thread * thread_id;
			double wig_x;
			for (dspin two_x = two_x_min_thread; two_x < two_x_max_thread; two_x += 2) {	

				wig_x = wig9jj_x(two_j1_l, two_j2_l, two_j3_l,
								 two_j4_l, two_j5_l, two_j6_l,
								 two_j7_l, two_j8_l, two_j9_l,
								 two_x, coupling);
				buf[bufi] = wig_x;
				bufi++;
				filled++;

			}

		}

		int tid = omp_get_thread_num();
		if (tid != 0) {
			// not in master thread
			wig_temp_free();
		}
		
		} // omp parallel

		// last xs
		if (x_last > 0) {

			double wig_x;
			for (dspin two_x = two_x_max - 2*(x_last-1); two_x <= two_x_max; two_x += 2) {	

				wig_x = wig9jj_x(two_j1_l, two_j2_l, two_j3_l,
								two_j4_l, two_j5_l, two_j6_l,
								two_j7_l, two_j8_l, two_j9_l,
								two_x, coupling);
				buf[filled] = wig_x;
				filled++;

			}

		}

		// check that we computed all x required
		if (filled != nx) {
			error("computed %i values but %i were expected", filled, (int)nx);
		}

		// print loop
		dspin two_x = two_x_min;
		for (int i = 0; i < nx; i++) {
			fprintf(file, "%i %.3e\n", two_x, buf[i]);
			two_x += 2;
		}
		
		fclose(file);
		free(buf);

		if (verbose) {
			printf(" done.\n");
		}

	}

	wig_temp_free(); 
	wig_table_free();

	return EXIT_SUCCESS;

}
