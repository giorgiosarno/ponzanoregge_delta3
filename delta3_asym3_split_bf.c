/* Copyright 2019 Giorgio Sarno, Pietro Donà and Francesco Gozzini */

/* sl2cfoam is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   sl2cfoam is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

// tool to compute the distribution of the summands
// in the sum of the irreducible and reducible 9j symbols

#include <math.h>
#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>

#include "sl2cfoam.h"
#include "common.h"
#include "error.h"
#include "dbg.h"
#include "utilities.h"
#include "wigxjpf.h"

#define FIRST_CRITICAL  0
#define SECOND_CRITICAL 1

#define MAX_J_WIG 4000

static double wig9jj_x(dspin two_j1, dspin two_j2, dspin two_j3,
					   dspin two_j4, dspin two_j5, dspin two_j6,
					   dspin two_j7, dspin two_j8, dspin two_j9,
					   dspin two_x) {

	double res;

	double prefactor;
	double bf_1, bf_2, bf_3;

	// use Biedenharn-Elliot formula

	
	prefactor = d(two_x) * real_negpow(two_x + 
									   two_j1 + two_j2 + two_j3 +
									   two_j4 + two_j5 + two_j6 +
									   two_j7 + two_j8 + two_j9);

	bf_1 = wig6jj(two_j5, two_j8, two_x,
					two_j9, two_j6, two_j1);

	bf_2 = wig6jj(two_j9, two_j6, two_x,
					two_j4, two_j7, two_j2);

	bf_3 = wig6jj(two_j4, two_j7, two_x,
					two_j8, two_j5, two_j3);

	res = prefactor * bf_1 * bf_2 * bf_3;

	return res;

}

int main(int argc, char **argv) {

	// spins
	dspin two_j1, two_j2, two_j3, 
		  two_j4, two_j5, two_j6,
		  two_j7, two_j8, two_j9;

	two_j1 = 2;
	two_j2 = 2;
	two_j3 = 2;
	two_j4 = 4;
	two_j5 = 4;
	two_j6 = 4;
	two_j7 = 2;
	two_j8 = 2;
	two_j9 = 2;

	double x_critical_1 = 1./3. * ( - sqrt(6)+sqrt(33));
	double x_critical_2 = 1./3. * (   sqrt(6)+sqrt(33));

	dspin two_x_range_min = max(max(abs(two_j5-two_j8),abs(two_j9-two_j6)),abs(two_j4-two_j7));
	dspin two_x_range_max = min(min(two_j5+two_j8,two_j9+two_j6),two_j4+two_j7);

	dspin window = 4;
	
	// lambdas
	int lambda_min, lambda_max, lambda_step;
	
	lambda_min= 1000;
	lambda_max = 1100;
	lambda_step = +1;

    // verbose flag
    bool verbose;
    verbose = false;

	int coupling = FIRST_CRITICAL;

	char filepath[1024];
	strcpy(filepath, "./delta3/data_delta3/"); // default directory

	// initialize wigxjpf
	wig_table_init(2*MAX_J_WIG, 6);
	wig_temp_init(2*MAX_J_WIG);

	// fileath for this lambda
	char filepath_l[1024];
	char filename[128];

	if (coupling == FIRST_CRITICAL) {
		sprintf(filename, "d3-asym3-split-bf_%i.%i.%i.%i.%i.%i.%i.%i.%i_%i_%i_FC.dat", 
				two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9,lambda_max,window);
	} 
	if (coupling == SECOND_CRITICAL) {
		sprintf(filename, "d3-asym3-split-bf_%i.%i.%i.%i.%i.%i.%i.%i.%i_%i_%i_SC.dat", 
				two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9,lambda_max,window);
	}
	if (coupling == EXPONENTIAL) {
		sprintf(filename, "d3-asym3-split-bf_%i.%i.%i.%i.%i.%i.%i.%i.%i_%i_%i_EX.dat", 
				two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9,lambda_max,window);
	}

	strcpy(filepath_l, filepath);
	strcat(filepath_l, filename);

	FILE *file = fopen(filepath_l, "w");
	if (file == NULL){
		error("error opening file for writing");
	}

	for (dspin lambda = lambda_min; lambda <= lambda_max; lambda += lambda_step) {

		if (verbose) {
			printf("Computing lambda = %i ...", lambda);
			fflush(stdout);
		}

		dspin two_j1_l, two_j2_l, two_j3_l, 
			  two_j4_l, two_j5_l, two_j6_l,
		      two_j7_l, two_j8_l, two_j9_l;
		
		two_j1_l = two_j1 * lambda;	
		two_j2_l = two_j2 * lambda;	  
		two_j3_l = two_j3 * lambda;	  
		two_j4_l = two_j4 * lambda;	  
		two_j5_l = two_j5 * lambda;	  
		two_j6_l = two_j6 * lambda;	  
		two_j7_l = two_j7 * lambda;	  
		two_j8_l = two_j8 * lambda;	  
		two_j9_l = two_j9 * lambda;

		// x boundaries
		dspin two_x_min, two_x_max;


		if (coupling == FIRST_CRITICAL) {

			dspin two_range_critical_min = (dspin)(lambda * 2 * x_critical_1) - 2* window * (dspin)(sqrt(lambda*(two_x_range_max - two_x_range_min)/2.)/2.);
			dspin two_range_critical_max = (dspin)(lambda * 2 * x_critical_1) + 2* window * (dspin)(sqrt(lambda*(two_x_range_max - two_x_range_min)/2.)/2.);

			if (two_range_critical_min % 2 != 0) {two_range_critical_min = two_range_critical_min + 1; }
			if (two_range_critical_max % 2 != 0) {two_range_critical_max = two_range_critical_max + 1; }

			two_x_min = max(lambda* two_x_range_min, two_range_critical_min);
			two_x_max = min(lambda * two_x_range_max, two_range_critical_max);
			//printf("%i %i\n", two_x_min,  two_x_max);
		}
		
		if (coupling == SECOND_CRITICAL) {

			dspin two_range_critical_min = (dspin)(lambda * 2 * x_critical_2) - 2* window * (dspin)(sqrt(lambda*(two_x_range_max - two_x_range_min)/2.)/2.);
			dspin two_range_critical_max = (dspin)(lambda * 2 * x_critical_2) + 2* window * (dspin)(sqrt(lambda*(two_x_range_max - two_x_range_min)/2.)/2.);

			if (two_range_critical_min % 2 != 0) {two_range_critical_min = two_range_critical_min + 1; }
			if (two_range_critical_max % 2 != 0) {two_range_critical_max = two_range_critical_max + 1; }

			two_x_min = max(lambda* two_x_range_min, two_range_critical_min);
			two_x_max = min(lambda * two_x_range_max, two_range_critical_max);
			//printf("%i %i\n", two_x_min,  two_x_max);

		} 
	

		// buffer array
		size_t nx = (two_x_max - two_x_min) / 2 + 1;
		double* buf = calloc(nx, sizeof(double));

		int nthreads;
		size_t x_per_thread;
		size_t x_last;
		int filled = 0;

		#pragma omp parallel reduction (+:filled)
        {

		wig_thread_temp_init(MAX_J_WIG);

		#pragma omp single
		{

		// get number of threads
		nthreads = omp_get_num_threads();

		// get the block of xs per each thread
		// there will be x missing to be filled at the end (division with remainder...)
		x_per_thread = nx / nthreads;
		x_last = nx % nthreads;

		}

		#pragma omp for
		for (int thread_id = 0; thread_id < nthreads; thread_id++) {

			dspin two_x_min_thread = two_x_min + 2*thread_id*x_per_thread;
			dspin two_x_max_thread = two_x_min_thread + 2*x_per_thread;

			int bufi = x_per_thread * thread_id;
			double wig_x;
			for (dspin two_x = two_x_min_thread; two_x < two_x_max_thread; two_x += 2) {	
				
				wig_x = wig9jj_x(two_j1_l, two_j2_l, two_j3_l,
								 two_j4_l, two_j5_l, two_j6_l,
								 two_j7_l, two_j8_l, two_j9_l,
								 two_x);

				buf[bufi] = wig_x;
				bufi++;
				filled++;

			}

		}

		int tid = omp_get_thread_num();
		if (tid != 0) {
			// not in master thread
			wig_temp_free();
		}
		
		} // omp parallel

		// last xs
		if (x_last > 0) {

			double wig_x;
			for (dspin two_x = two_x_max - 2*(x_last-1); two_x <= two_x_max; two_x += 2) {	

				wig_x = wig9jj_x(two_j1_l, two_j2_l, two_j3_l,
								two_j4_l, two_j5_l, two_j6_l,
								two_j7_l, two_j8_l, two_j9_l,
								two_x);
				buf[filled] = wig_x;
				filled++;

			}

		}

		// check that we computed all x required
		if (filled != nx) {
			error("computed %i values but %i were expected", filled, (int)nx);
		}

		// sum loop
		dspin two_x = two_x_min;
		double result = 0.0;
		for (int i = 0; i < nx; i++) {
			result += buf[i];
		}
		
		fprintf(file, "%i %.3e\n", lambda, result);

		free(buf);

		if (verbose) {
			printf(" done.\n");
		}

	}

	fclose(file);

	wig_temp_free(); 
	wig_table_free();

	return EXIT_SUCCESS;

}
